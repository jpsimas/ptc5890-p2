# PTC5890-P2 Solution to Computational Part of First Exam of PCS5890
# Copyright (C) 2021  João Pedro de Omena Simas

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
from math import pi

import matplotlib.pyplot as plt

def runSim(mu, n, nRealiz):
    MSE = np.zeros(n - nW)

    for realiz in range(0, nRealiz):
        phi = np.random.uniform(0, 2*pi)
        s = std_s*np.random.normal(size = n)
        d = np.sin(2*pi/10*np.arange(n) + pi/6 + phi) + s
        u = 5*np.sin(2*pi/10*np.arange(n) + phi)

        e = np.zeros(n - nW)
        
        w = np.zeros((n - nW, nW))
        wi = np.zeros(nW)
    
        for i in range(0, n - nW):
            ui = u[i:(i + nW)]
            e[i] = d[i + nW - 1] - np.dot(wi, ui)
            #note that, for the sake of simplicity, w is inverted
            w[i] = wi
            wi += mu*e[i]*ui
        MSE += e*e
        
    MSE /= nRealiz
    return w, e, MSE, u, s

def runSims(mus, n, nRealiz, name):
    MSEss = np.zeros(len(mus))
    for k in range(0, len(mus)):
        mu = mus[k]
        w, e, MSE, u, s = runSim(mu, n, nRealiz)

        MSEss[k] = (10*np.log10(np.mean(MSE[100:])))
        print("MSEss = {}".format(MSEss[k]) + " dB")

        plt.figure(1)
        plt.plot(10*np.log10(MSE), label = "mu = {:.2f}".format(mu))
        
        plt.figure(0)
        w = np.flip(w, axis = 1)
        plt.plot(w[:, 0], w[:, 1], label = "mu = {:.2f}".format(mu))

    plt.figure(2)
    plt.plot(u, label = "u(i)")
    plt.plot(e, label = "e(i)")
    plt.plot(s, label = "s(i)")

    plt.figure(1)
    plt.xlabel("Discrete Time")
    plt.ylabel("MSE (dB)")
    if(len(mus) != 1):
        plt.legend()
    plt.savefig("../img/MSE" + name + ".svg")

    plt.figure(0)
    plt.plot(wo[0], wo[1], "rx")
    plt.xlabel("$w_0$")
    plt.ylabel("$w_1$")
    plt.xlim((-0.5, 0.5))
    plt.ylim((-0.5, 0.5))
    if(len(mus) != 1):
        plt.legend()
    plt.savefig("../img/w" + name + ".svg")

    plt.figure(4)
    fig, axs = plt.subplots(2)
    axs[0].plot(w[:, 0], label = "$w(i)_0$")
    axs[1].plot(w[:, 1], label = "$w(i)_1$")
    axs[0].plot(np.full(n, wo[0]), "r", label = "$w_{o0}$")
    axs[1].plot(np.full(n, wo[1]), "r", label = "$w_{o1}$")
    plt.xlabel("Discrete Time")
    axs[0].legend()
    axs[1].legend()
    plt.savefig("../img/wtime" + name + ".svg")

    plt.figure(2)
    plt.xlabel("Discrete Time")
    plt.legend()
    plt.savefig("../img/sigs" + name + ".svg")
    
    return MSEss, w

def drawContour(R, nPts = 101):
    # Draw contour lines
    unit = np.array([np.cos(2*pi*np.arange(nPts)/(nPts - 1)), np.sin(2*pi*np.arange(nPts)/(nPts - 1))]).T
    unit = np.matrix(unit)*np.matrix(np.linalg.inv(np.sqrt(R)))

    plt.figure(0)
    for J in np.logspace(-2, 1, 7, base = 10):
        rSq = np.sqrt(J - Jmin)
        curv = np.matrix(wo) + rSq*unit
        plt.plot(curv[:, 0], curv[:, 1], "0.8")
        plt.text(curv[62*nPts//100, 0], curv[62*nPts//100, 1], "{:.1f}".format(10*np.log10(J)) + "dB", color = "black", ha = "center", size = 8.0, rotation = 30)

    # plt.text(wo[0], wo[1], 'MSE = ' + "{:.1f}".format(10*np.log10(Jmin)) + "dB", color = "black")


print("PTC5890-P2  Copyright (C) 2021  João Pedro de O. Simas\n"\
      "This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.\n"\
      "This is free software, and you are welcome to redistribute it\n"\
      "under certain conditions.\n")

plt.close("all")
plt.rc("text", usetex = True)
# plt.ion()

# Theoretical Results
nW = 2
var_s = 0.01
std_s = np.sqrt(var_s)

R = np.zeros((nW, nW))
p = np.zeros(nW)
for i in range(0, nW):
    p[i] = 5/2*np.cos(2*pi*i/10 + pi/6)
    for k in range(0, nW):
        R[i, k] = 25/2 * np.cos(2*pi*(i - k)/10)

wo = np.linalg.solve(R, p)

var_d = var_s + 1/2

Jmin = var_s

# eigenvalues
[lR, vR] = np.linalg.eig(R)
print("eigenvalues:")
print(lR)
lMax = np.max(lR) 
print("lambda_max = {}".format(lMax))
print("mu_max = {}".format(2/lMax))

# theoretical EMSE for mu = 0.03
mu = 0.03
EMSELMS = mu * var_s * np.trace(R)/(2 - mu*np.trace(R))
misadjLMS = EMSELMS/var_s

print("EMSELMS = {} dB".format(10*np.log10(EMSELMS)))
print("misadjLMS = {} dB".format(10*np.log10(misadjLMS)))

# Simulation
n = 500
nRealiz = 1
mus = [0.03]

drawContour(R)
MSEss1s, w1s = runSims(mus, n, nRealiz, name = "1single")

plt.close("all")

n = 500
nRealiz = 500
mus = [0.03]

drawContour(R)
MSEss1, w1 = runSims(mus, n, nRealiz, name = "1")

plt.close("all")

n = 500
nRealiz = 500
mus = [0.01, 0.03, 0.05]

plt.close("all")

drawContour(R)
MSEss3, w3 = runSims(mus, n, nRealiz, name = "3")

#determine max mu
nRealiz = 100
n = 500
mu = 0.065

plt.figure(3)

while True:
    w, e, MSE, u, s = runSim(mu, n, nRealiz)
    plt.plot(10*np.log10(MSE), label = "MSE = {}".format(mu))
    if(MSE[-1] > 10*MSE[0]):
        break
    else:
        mu += 0.001

print("mu_max = {}".format(mu))

plt.xlabel("Discrete Time")
plt.ylabel("MSE (dB)")
plt.legend()
plt.savefig("../img/conv.svg")

# plt.show()
